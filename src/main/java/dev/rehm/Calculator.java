package dev.rehm;

public class Calculator {

    public int add(String input) {
        if(input==null || input.isEmpty()){
            return 0;
        }
        String[] inputArr = input.split(",");
        if(inputArr.length==1){
            return Integer.parseInt(inputArr[0]);
        }
        if(inputArr.length>2) {
            throw new IllegalArgumentException();
        }
        return Integer.parseInt(inputArr[0])+Integer.parseInt(inputArr[1]);
    }


}
