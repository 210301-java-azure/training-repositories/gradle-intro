package dev.rehm;

/*
    create a string calculator with an add method which returns an int
    - the add method should take up to two numbers, separated by a comma
    - a single number is returned, two numbers are added and the sum is returned
    - an empty string should return 0
    - an exception is thrown if invalid input
 */

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    // annotations - @BeforeEach, @BeforeAll, @AfterEach, @AfterAll, @Test, @Disabled

    private Calculator calculator = new Calculator();

    @Test
    public void testEmptyString(){
        int expected = 0;
        int actual = calculator.add("");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testThreeInputs(){
        Assertions.assertThrows(IllegalArgumentException.class,
                ()->{calculator.add("1,2,3");});
    }

    @Test
    public void testTwoInputs(){
//        int expected = 5;
//        int actual = calculator.add("2,3");
//        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(5, calculator.add("2,3"));
    }

    @Test
    @Disabled
    public void testOneInput(){
        Assertions.assertEquals(5, calculator.add("5"));
    }


}
